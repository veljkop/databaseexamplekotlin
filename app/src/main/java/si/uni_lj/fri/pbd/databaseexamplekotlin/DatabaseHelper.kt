package si.uni_lj.fri.pbd.databaseexamplekotlin

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class DatabaseHelper(context: Context): SQLiteOpenHelper(
        context, DATABASE_NAME, null, DATABASE_VERSION)  {

    companion object {
        const val TAG = "DatabaseHelper"
        const val DATABASE_VERSION = 2
        const val DATABASE_NAME = "contacts_db"
        const val TABLE_CONTACTS = "contacts"
        const val USER_NAME = "name"
        const val USER_EMAIL = "email"
        const val _ID = "_id"
        val COLUMNS = arrayOf(_ID, USER_NAME, USER_EMAIL)
    }

    override fun onCreate(db: SQLiteDatabase?) {

        Log.d(TAG, "onCreate")
        val CREATE_CONTACTS_TABLE = ("CREATE TABLE "+ TABLE_CONTACTS+"("
                + _ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "
                + USER_NAME + " TEXT NOT NULL,"
                + USER_EMAIL + " TEXT NOT NULL)")
        db?.execSQL(CREATE_CONTACTS_TABLE)

    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        Log.d(TAG, "onUpgrade")

        db?.execSQL("DROP TABLE IF EXISTS "+ TABLE_CONTACTS)
        onCreate(db)
    }

    fun deleteDatabase(context: Context) {
        Log.d(TAG, "deleteDatabase")

        context.deleteDatabase(DATABASE_NAME)
    }
}