package si.uni_lj.fri.pbd.databaseexamplekotlin

import android.content.ContentValues
import android.database.Cursor
import android.os.Bundle
import android.util.Log
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import androidx.cursoradapter.widget.SimpleCursorAdapter
import si.uni_lj.fri.pbd.databaseexamplekotlin.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    var dbHelper: DatabaseHelper? = null
    var adapter: SimpleCursorAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        dbHelper = DatabaseHelper(this)

        clearAll()

        insertContacts()

        val cursor: Cursor? = readContacts()

        adapter = SimpleCursorAdapter(this, R.layout.list_layout,
                cursor, DatabaseHelper.COLUMNS, intArrayOf(R.id._id, R.id.name, R.id.name),0)
        binding.list.adapter = adapter
        binding.fixButton.setOnClickListener {
            fix()
            adapter?.cursor?.requery()
            adapter?.notifyDataSetChanged()
        }
    }
    companion object{
        const val TAG = "MainActivity"
    }

    private fun insertContacts() {
        val values: ContentValues = ContentValues()
        values.put(DatabaseHelper.USER_NAME, "Veljko Pejovic");
        values.put(DatabaseHelper.USER_EMAIL, "Veljko.Pejovic@fri.uni-lj.si");
        dbHelper?.writableDatabase?.insert(DatabaseHelper.TABLE_CONTACTS, null, values)
        values.clear()

        values.put(DatabaseHelper.USER_NAME, "Mojca Ciglaric");
        values.put(DatabaseHelper.USER_EMAIL, "Mojca.Ciglaric@fri.uni-lj.si");
        dbHelper?.writableDatabase?.insert(DatabaseHelper.TABLE_CONTACTS, null, values)
        values.clear()

        values.put(DatabaseHelper.USER_NAME, "Octavian Machidon");
        values.put(DatabaseHelper.USER_EMAIL, "Octavian@fri.uni-lj.si");
        dbHelper?.writableDatabase?.insert(DatabaseHelper.TABLE_CONTACTS, null, values)
        values.clear()

        values.put(DatabaseHelper.USER_NAME, "Xyz");
        values.put(DatabaseHelper.USER_EMAIL, "Corrupt data");
        dbHelper?.writableDatabase?.insert(DatabaseHelper.TABLE_CONTACTS, null, values)
        values.clear()
    }

    private fun readContacts(): Cursor? {
        return dbHelper?.readableDatabase?.query(DatabaseHelper.TABLE_CONTACTS, DatabaseHelper.COLUMNS,
                null, null, null, null, null)
    }


    private fun fix() {
        dbHelper?.writableDatabase?.delete(DatabaseHelper.TABLE_CONTACTS,
        DatabaseHelper.USER_NAME + "=?", arrayOf("Xyz"))

        val values: ContentValues = ContentValues()
        values.put(DatabaseHelper.USER_NAME, "Octavian Machidon");
        values.put(DatabaseHelper.USER_EMAIL, "Octavian.Machidon@fri.uni-lj.si");
        dbHelper?.writableDatabase?.update(DatabaseHelper.TABLE_CONTACTS, values,
        DatabaseHelper.USER_NAME+ "=?", arrayOf("Octavian Machidon"))
    }


    private fun clearAll() {
        dbHelper?.writableDatabase?.delete(DatabaseHelper.TABLE_CONTACTS, null, null)
    }

    override fun onDestroy() {
        dbHelper?.writableDatabase?.close()
        dbHelper?.deleteDatabase(this)

        super.onDestroy()
    }
}